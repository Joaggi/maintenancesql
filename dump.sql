--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: maintenance; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE maintenance WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE maintenance OWNER TO postgres;

\connect maintenance

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: department; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE department (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    name character varying
);


ALTER TABLE department OWNER TO postgres;

--
-- Name: department_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE department_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE department_id_seq OWNER TO postgres;

--
-- Name: department_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE department_id_seq OWNED BY department.id;


--
-- Name: employee; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE employee (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    name character varying,
    last_name character varying,
    identification character varying,
    gender character(11),
    job_position character varying,
    salary real,
    fk_department integer
);


ALTER TABLE employee OWNER TO postgres;

--
-- Name: employee_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE employee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE employee_id_seq OWNER TO postgres;

--
-- Name: employee_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE employee_id_seq OWNED BY employee.id;


--
-- Name: machine; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE machine (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    description character varying,
    serial_number character varying,
    machine_number character varying,
    adquisition_date date,
    voltage real,
    model character varying,
    fk_machine_type integer,
    fk_machine_brand integer,
    fk_machine_category integer
);


ALTER TABLE machine OWNER TO postgres;

--
-- Name: machine_brand; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE machine_brand (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    name character varying
);


ALTER TABLE machine_brand OWNER TO postgres;

--
-- Name: machine_brand_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE machine_brand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE machine_brand_id_seq OWNER TO postgres;

--
-- Name: machine_brand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE machine_brand_id_seq OWNED BY machine_brand.id;


--
-- Name: machine_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE machine_category (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    name character varying,
    description character varying
);


ALTER TABLE machine_category OWNER TO postgres;

--
-- Name: machine_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE machine_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE machine_category_id_seq OWNER TO postgres;

--
-- Name: machine_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE machine_category_id_seq OWNED BY machine_category.id;


--
-- Name: machine_failure; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE machine_failure (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    name character varying,
    description character varying
);


ALTER TABLE machine_failure OWNER TO postgres;

--
-- Name: machine_failure_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE machine_failure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE machine_failure_id_seq OWNER TO postgres;

--
-- Name: machine_failure_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE machine_failure_id_seq OWNED BY machine_failure.id;


--
-- Name: machine_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE machine_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE machine_id_seq OWNER TO postgres;

--
-- Name: machine_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE machine_id_seq OWNED BY machine.id;


--
-- Name: machine_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE machine_type (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    name character varying,
    description character varying
);


ALTER TABLE machine_type OWNER TO postgres;

--
-- Name: machine_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE machine_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE machine_type_id_seq OWNER TO postgres;

--
-- Name: machine_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE machine_type_id_seq OWNED BY machine_type.id;


--
-- Name: maintenance; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE maintenance (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    fk_machine_failure integer,
    priority character varying,
    status character varying,
    name character varying,
    description character varying,
    fk_maintenance_type integer,
    standard_hours integer,
    due_date timestamp without time zone,
    start_date timestamp without time zone,
    fk_machine integer
);


ALTER TABLE maintenance OWNER TO postgres;

--
-- Name: maintenance_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE maintenance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE maintenance_id_seq OWNER TO postgres;

--
-- Name: maintenance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE maintenance_id_seq OWNED BY maintenance.id;


--
-- Name: maintenance_report; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE maintenance_report (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    name character varying,
    machines character varying
);


ALTER TABLE maintenance_report OWNER TO postgres;

--
-- Name: maintenance_report_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE maintenance_report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE maintenance_report_id_seq OWNER TO postgres;

--
-- Name: maintenance_report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE maintenance_report_id_seq OWNED BY maintenance_report.id;


--
-- Name: maintenance_task; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE maintenance_task (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    name character varying,
    description character varying,
    fk_maintenance integer
);


ALTER TABLE maintenance_task OWNER TO postgres;

--
-- Name: maintenance_task_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE maintenance_task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE maintenance_task_id_seq OWNER TO postgres;

--
-- Name: maintenance_task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE maintenance_task_id_seq OWNED BY maintenance_task.id;


--
-- Name: maintenance_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE maintenance_type (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    name character varying,
    description character varying,
    standard_hours integer
);


ALTER TABLE maintenance_type OWNER TO postgres;

--
-- Name: maintenance_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE maintenance_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE maintenance_type_id_seq OWNER TO postgres;

--
-- Name: maintenance_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE maintenance_type_id_seq OWNED BY maintenance_type.id;


--
-- Name: request_spare_part; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE request_spare_part (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    fk_spare_part integer
);


ALTER TABLE request_spare_part OWNER TO postgres;

--
-- Name: request_spare_part_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE request_spare_part_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE request_spare_part_id_seq OWNER TO postgres;

--
-- Name: request_spare_part_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE request_spare_part_id_seq OWNED BY request_spare_part.id;


--
-- Name: schedule_maintenance; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE schedule_maintenance (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    date date,
    employee integer
);


ALTER TABLE schedule_maintenance OWNER TO postgres;

--
-- Name: schedule_maintenance_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE schedule_maintenance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE schedule_maintenance_id_seq OWNER TO postgres;

--
-- Name: schedule_maintenance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE schedule_maintenance_id_seq OWNED BY schedule_maintenance.id;


--
-- Name: spare_part; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE spare_part (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    name character varying,
    description character varying
);


ALTER TABLE spare_part OWNER TO postgres;

--
-- Name: spare_part_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE spare_part_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spare_part_id_seq OWNER TO postgres;

--
-- Name: spare_part_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE spare_part_id_seq OWNED BY spare_part.id;


--
-- Name: work_place; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE work_place (
    id integer NOT NULL,
    create_date timestamp without time zone,
    last_modified_date timestamp without time zone,
    name character varying,
    description character varying
);


ALTER TABLE work_place OWNER TO postgres;

--
-- Name: work_place_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE work_place_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE work_place_id_seq OWNER TO postgres;

--
-- Name: work_place_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE work_place_id_seq OWNED BY work_place.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY department ALTER COLUMN id SET DEFAULT nextval('department_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY employee ALTER COLUMN id SET DEFAULT nextval('employee_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY machine ALTER COLUMN id SET DEFAULT nextval('machine_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY machine_brand ALTER COLUMN id SET DEFAULT nextval('machine_brand_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY machine_category ALTER COLUMN id SET DEFAULT nextval('machine_category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY machine_failure ALTER COLUMN id SET DEFAULT nextval('machine_failure_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY machine_type ALTER COLUMN id SET DEFAULT nextval('machine_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY maintenance ALTER COLUMN id SET DEFAULT nextval('maintenance_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY maintenance_report ALTER COLUMN id SET DEFAULT nextval('maintenance_report_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY maintenance_task ALTER COLUMN id SET DEFAULT nextval('maintenance_task_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY maintenance_type ALTER COLUMN id SET DEFAULT nextval('maintenance_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY request_spare_part ALTER COLUMN id SET DEFAULT nextval('request_spare_part_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY schedule_maintenance ALTER COLUMN id SET DEFAULT nextval('schedule_maintenance_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY spare_part ALTER COLUMN id SET DEFAULT nextval('spare_part_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY work_place ALTER COLUMN id SET DEFAULT nextval('work_place_id_seq'::regclass);


--
-- Data for Name: department; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO department VALUES (4, '2017-08-28 14:30:23.557', '2017-08-28 14:30:23.557', 'Terminacion');
INSERT INTO department VALUES (5, '2017-08-29 17:31:52.22', '2017-08-29 17:31:52.22', 'Contabilidad');
INSERT INTO department VALUES (6, '2017-10-10 18:20:28.774', '2017-10-10 18:20:28.774', 'Recursos Humanos');
INSERT INTO department VALUES (7, '2017-10-10 18:20:32.284', '2017-10-10 18:20:32.284', 'Gerencia');


--
-- Name: department_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('department_id_seq', 7, true);


--
-- Data for Name: employee; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO employee VALUES (3, '2017-08-29 17:46:37.611', '2017-08-29 17:46:37.611', 'asdf', 'asdf', 'asdf', 'asdf       ', 'asdf', 234, 5);
INSERT INTO employee VALUES (4, '2017-10-10 18:20:53.312', '2017-10-10 18:20:53.312', 'Joseph', 'Gallego', '1022369610', 'Masculino  ', 'Operario', 20000000, 7);
INSERT INTO employee VALUES (5, '2017-10-10 18:21:08.436', '2017-10-10 18:21:08.436', 'Cristian', 'Mejia', '1128265603', 'Masculino  ', 'Operario', 2350000, 4);
INSERT INTO employee VALUES (6, '2017-10-10 18:21:51.967', '2017-10-10 18:21:51.967', 'Daniela', 'Martin', '1018465807', 'Femenino   ', 'Mecánico', NULL, 6);
INSERT INTO employee VALUES (7, '2017-10-10 18:22:31.275', '2017-10-10 18:22:31.275', 'Flor', 'Vega', '21660268', 'Femenino   ', 'Mecánico', NULL, 7);


--
-- Name: employee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('employee_id_seq', 7, true);


--
-- Data for Name: machine; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: machine_brand; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: machine_brand_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('machine_brand_id_seq', 1, false);


--
-- Data for Name: machine_category; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: machine_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('machine_category_id_seq', 1, false);


--
-- Data for Name: machine_failure; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: machine_failure_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('machine_failure_id_seq', 1, false);


--
-- Name: machine_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('machine_id_seq', 1, false);


--
-- Data for Name: machine_type; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: machine_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('machine_type_id_seq', 1, false);


--
-- Data for Name: maintenance; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: maintenance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('maintenance_id_seq', 1, false);


--
-- Data for Name: maintenance_report; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: maintenance_report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('maintenance_report_id_seq', 1, false);


--
-- Data for Name: maintenance_task; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: maintenance_task_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('maintenance_task_id_seq', 1, false);


--
-- Data for Name: maintenance_type; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: maintenance_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('maintenance_type_id_seq', 1, false);


--
-- Data for Name: request_spare_part; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO request_spare_part VALUES (1, '2017-08-29 18:07:20.908', '2017-08-29 18:07:20.908', NULL);
INSERT INTO request_spare_part VALUES (2, '2017-08-29 18:09:40.827', '2017-08-29 18:09:40.827', 1);


--
-- Name: request_spare_part_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('request_spare_part_id_seq', 2, true);


--
-- Data for Name: schedule_maintenance; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: schedule_maintenance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('schedule_maintenance_id_seq', 1, false);


--
-- Data for Name: spare_part; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO spare_part VALUES (1, '2017-08-29 17:57:03.947', '2017-08-29 17:57:03.947', 'asdf', NULL);
INSERT INTO spare_part VALUES (2, '2017-08-29 18:07:12.303', '2017-08-29 18:07:12.303', 'ddd', NULL);


--
-- Name: spare_part_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('spare_part_id_seq', 2, true);


--
-- Data for Name: work_place; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO work_place VALUES (1, '2017-08-29 17:55:21.085', '2017-08-29 17:55:21.085', 'asdf', NULL);


--
-- Name: work_place_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('work_place_id_seq', 1, true);


--
-- Name: pk_department; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY department
    ADD CONSTRAINT pk_department PRIMARY KEY (id);


--
-- Name: pk_employee; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY employee
    ADD CONSTRAINT pk_employee PRIMARY KEY (id);


--
-- Name: pk_machine; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY machine
    ADD CONSTRAINT pk_machine PRIMARY KEY (id);


--
-- Name: pk_machine_brand; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY machine_brand
    ADD CONSTRAINT pk_machine_brand PRIMARY KEY (id);


--
-- Name: pk_machine_category; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY machine_category
    ADD CONSTRAINT pk_machine_category PRIMARY KEY (id);


--
-- Name: pk_machine_failure; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY machine_failure
    ADD CONSTRAINT pk_machine_failure PRIMARY KEY (id);


--
-- Name: pk_machine_type; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY machine_type
    ADD CONSTRAINT pk_machine_type PRIMARY KEY (id);


--
-- Name: pk_maintenance; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY maintenance
    ADD CONSTRAINT pk_maintenance PRIMARY KEY (id);


--
-- Name: pk_maintenance_report; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY maintenance_report
    ADD CONSTRAINT pk_maintenance_report PRIMARY KEY (id);


--
-- Name: pk_maintenance_task; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY maintenance_task
    ADD CONSTRAINT pk_maintenance_task PRIMARY KEY (id);


--
-- Name: pk_maintenance_type; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY maintenance_type
    ADD CONSTRAINT pk_maintenance_type PRIMARY KEY (id);


--
-- Name: pk_request_spare_part; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY request_spare_part
    ADD CONSTRAINT pk_request_spare_part PRIMARY KEY (id);


--
-- Name: pk_schedule_maintenance; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY schedule_maintenance
    ADD CONSTRAINT pk_schedule_maintenance PRIMARY KEY (id);


--
-- Name: pk_spare_part; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY spare_part
    ADD CONSTRAINT pk_spare_part PRIMARY KEY (id);


--
-- Name: pk_work_place; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY work_place
    ADD CONSTRAINT pk_work_place PRIMARY KEY (id);


--
-- Name: rel_employee_department; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY employee
    ADD CONSTRAINT rel_employee_department FOREIGN KEY (fk_department) REFERENCES department(id) MATCH FULL;


--
-- Name: rel_machine_machine_brand; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY machine
    ADD CONSTRAINT rel_machine_machine_brand FOREIGN KEY (fk_machine_brand) REFERENCES machine_brand(id) MATCH FULL;


--
-- Name: rel_machine_machine_categorie; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY machine
    ADD CONSTRAINT rel_machine_machine_categorie FOREIGN KEY (fk_machine_category) REFERENCES machine_category(id) MATCH FULL;


--
-- Name: rel_machine_machine_type; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY machine
    ADD CONSTRAINT rel_machine_machine_type FOREIGN KEY (fk_machine_type) REFERENCES machine_type(id) MATCH FULL;


--
-- Name: rel_maintenance_failure; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY maintenance
    ADD CONSTRAINT rel_maintenance_failure FOREIGN KEY (fk_machine_failure) REFERENCES machine_failure(id) MATCH FULL;


--
-- Name: rel_maintenance_machine; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY maintenance
    ADD CONSTRAINT rel_maintenance_machine FOREIGN KEY (fk_machine) REFERENCES machine(id) MATCH FULL;


--
-- Name: rel_maintenance_machine_type; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY maintenance
    ADD CONSTRAINT rel_maintenance_machine_type FOREIGN KEY (fk_maintenance_type) REFERENCES maintenance_type(id) MATCH FULL;


--
-- Name: rel_maintenance_task_maintenance; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY maintenance_task
    ADD CONSTRAINT rel_maintenance_task_maintenance FOREIGN KEY (fk_maintenance) REFERENCES maintenance(id) MATCH FULL;


--
-- Name: rel_request_spare_part_spare_part; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY request_spare_part
    ADD CONSTRAINT rel_request_spare_part_spare_part FOREIGN KEY (fk_spare_part) REFERENCES spare_part(id) MATCH FULL;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

